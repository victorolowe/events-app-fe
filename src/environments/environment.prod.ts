export const environment = {
  production: true,
  openWeatherApiKey: '<openWeatherApiKey>',
  geocodeApiKey: '<geocodeApiKey>',
  apiBaseUrl: '<apiBaseUrl>',
  aws: {
    region: '<region>',
    userPoolId: '<userPoolId>',
    userPoolWebClientId: '<userPoolWebClientId>',
    authenticationFlowType: 'USER_PASSWORD_AUTH',
  },
};
