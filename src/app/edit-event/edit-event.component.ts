import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IEvent } from '../event';
import { EventService } from '../event.service';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css'],
})
export class EditEventComponent implements OnInit {
  event?: IEvent;
  loading = true;

  constructor(private route: ActivatedRoute, private eventService: EventService) {}

  getEvent(id: string): void {
    this.eventService.getEvent(id).subscribe(({ event }) => {
      this.event = {
        ...event,
        date: new Date(event.date),
      };
      this.loading = false;
    });
  }

  ngOnInit(): void {
    const eventId = this.route.snapshot.paramMap.get('id') as string;
    this.getEvent(eventId);
  }
}
