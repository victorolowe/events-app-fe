import { Injectable } from '@angular/core';
import { Auth } from 'aws-amplify';
import { Observable, of } from 'rxjs';
import { IAuth } from './auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  error?: string;

  async authenticateUser(auth: IAuth): Promise<Observable<boolean>> {
    let res = of(false);

    try {
      const { Session } = await Auth.signIn(auth.email, auth.password);

      if (Session !== null) {
        res = of(true);
      }
    } catch (error) {
      this.error = 'Authentication failed';
    }

    return res;
  }

  clearError() {
    this.error = undefined;
  }
}
