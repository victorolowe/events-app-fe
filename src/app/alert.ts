export interface IAlert {
  title: string;
  message: string;
  primaryActionLabel: string;
  cancelActionLabel?: string;
  onPrimaryActionClick: () => void;
}
