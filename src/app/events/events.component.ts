import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert.service';
import { IEvent } from '../event';
import { EventService } from '../event.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
})
export class EventsComponent implements OnInit {
  events: IEvent[] = [];
  loading = true;

  constructor(private eventService: EventService, private alertService: AlertService) {}

  getEvents(): void {
    this.loading = true;
    this.eventService.getEvents().subscribe(({ events }) => {
      this.events = events;
      this.loading = false;
    });
  }

  deleteEvent(event: IEvent): void {
    this.eventService.deleteEvent(event.id).subscribe(() => {
      this.alertService.clear();
      this.getEvents();
    });
  }

  onDelete(event: IEvent): void {
    this.alertService.create({
      title: 'Delete Event',
      message: `Are you sure you want to delete the event: ${event.name}`,
      primaryActionLabel: 'Delete',
      cancelActionLabel: 'Cancel',
      onPrimaryActionClick: () => this.deleteEvent(event),
    });
  }

  ngOnInit(): void {
    this.getEvents();
  }
}
