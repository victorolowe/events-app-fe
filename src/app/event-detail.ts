export interface IEventDetail {
  name: string;
  date: any;
  location: string;
  expectedWeather: string;
}
