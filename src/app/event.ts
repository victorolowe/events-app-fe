export interface IEvent {
  id: string;
  name: string;
  date: Date;
  location: string;
  expectedWeather: string;
}
