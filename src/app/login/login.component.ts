import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IAuth } from '../auth';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  auth: IAuth = {
    email: '',
    password: '',
  };
  submitted = false;

  constructor(private router: Router, public authService: AuthService) {}

  async onSubmit(): Promise<void> {
    this.submitted = true;

    (await this.authService.authenticateUser(this.auth)).subscribe((res) => {
      if (res) {
        this.authService.clearError();
        this.router.navigate(['events']);
      }

      this.submitted = false;
    });
  }

  ngOnInit(): void {}
}
