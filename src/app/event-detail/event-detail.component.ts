import { Component, OnInit, Input } from '@angular/core';
import { IEvent } from '../event';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { IEventDetail } from '../event-detail';
import { EventService } from '../event.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css'],
})
export class EventDetailComponent implements OnInit {
  @Input() event?: IEvent;

  eventDetail: IEventDetail = {
    name: '',
    date: {},
    location: '',
    expectedWeather: '',
  };
  enableForecastPrediction = true;
  forecastError?: string;
  loadingForecast = false;
  submitted = false;

  constructor(
    private eventService: EventService,
    private router: Router,
    private location: Location
  ) {}

  private createNgbDateObject(date: Date) {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    return {
      year,
      month,
      day,
    };
  }

  private createDateFromNgbDateObject(date: any) {
    const { year, month, day } = date;
    return new Date(year, month - 1, day);
  }

  isDisabled = (date: NgbDate): boolean => {
    const compareDate = this.createNgbDateObject(new Date());
    return date.before(compareDate) || date.equals(compareDate);
  };

  onLoadWeatherForecast(): void {
    if (this.eventDetail.date === null || !this.eventDetail.date.year) {
      this.enableForecastPrediction = false;
      this.forecastError = 'Event weather forecast cannot be loaded because the date is not valid';
      return;
    }

    if (!this.eventDetail.location) {
      this.enableForecastPrediction = false;
      this.forecastError =
        'Event weather forecast cannot be loaded because there is  no event location specified';
      return;
    }

    const date = this.eventDetail.date;
    const currentDate = new Date();
    const compareDate = new Date(date.year, date.month - 1, date.day);
    const diff = Math.floor(
      (compareDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24)
    );

    if (diff > 7) {
      this.enableForecastPrediction = false;
      this.forecastError =
        'Event weather forecast cannot be loaded because it is more than 7 days in the future';
      return;
    }

    this.enableForecastPrediction = true;
    this.loadingForecast = true;
    this.forecastError = undefined;

    this.eventService.loadForecast(this.eventDetail.location).subscribe((forecast) => {
      this.eventDetail.expectedWeather = forecast.daily[0].weather[0].main;
      this.loadingForecast = false;
    });
  }

  goBack() {
    this.location.back();
  }

  onSubmit(): void {
    this.submitted = true;

    const fmtEventDetail = {
      ...this.eventDetail,
      date: this.createDateFromNgbDateObject(this.eventDetail.date),
    };

    if (this.event) {
      this.eventService.updateEvent(this.event.id, fmtEventDetail).subscribe(() => this.goBack());
    } else {
      this.eventService.createEvent(fmtEventDetail).subscribe(() => {
        this.router.navigate(['events']);
        this.submitted = false;
      });
    }
  }

  ngOnInit(): void {
    if (this.event) {
      this.eventDetail = {
        name: this.event.name,
        date: {
          year: this.event.date.getFullYear(),
          month: this.event.date.getMonth() + 1,
          day: this.event.date.getDate(),
        },
        location: this.event.location,
        expectedWeather: this.event.expectedWeather,
      };
    } else {
      this.eventDetail.date = this.createNgbDateObject(new Date());
    }
  }
}
