import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IAlert } from '../alert';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css'],
})
export class AlertComponent implements OnInit {
  alert?: IAlert;

  constructor(public activeModal: NgbActiveModal) {}

  onPrimaryClick(): void {
    if (this.alert) {
      this.alert.onPrimaryActionClick();
    }
  }

  ngOnInit(): void {}
}
