import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IAlert } from './alert';
import { AlertComponent } from './alert/alert.component';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(private modalService: NgbModal) {}

  create(alert: IAlert): void {
    const modalRef = this.modalService.open(AlertComponent, {
      ariaLabelledBy: 'modal-basic-title',
    });
    modalRef.componentInstance.alert = alert;
  }

  clear(): void {
    this.modalService.dismissAll();
  }
}
