import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, concatMap, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { IEvent } from './event';
import { AlertService } from './alert.service';
import { environment } from 'src/environments/environment';
import { IEventDetail } from './event-detail';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  private eventsUrl = `${environment.apiBaseUrl}/event`;
  private weatherUrl = `https://api.openweathermap.org/data/2.5/onecall?exclude=current,minutely,hourly,alerts&appid=${environment.openWeatherApiKey}`;
  geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?key=${environment.geocodeApiKey}`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  weatherForecast?: string;

  constructor(private http: HttpClient, private alertService: AlertService) {}

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      let errorMessage;
      switch (operation) {
        case 'loadForecast':
          errorMessage = error.error.message;
          break;
        default:
          errorMessage = error.message;
          break;
      }

      this.processError(`${operation} failed: ${errorMessage}`);
      return of(result as T);
    };
  }

  getEvents(): Observable<any> {
    return this.http
      .get<any>(`${this.eventsUrl}s`)
      .pipe(catchError(this.handleError<any>('getEvents', [])));
  }

  getEvent(id: string): Observable<any> {
    return this.http
      .get<any>(`${this.eventsUrl}/${id}`, this.httpOptions)
      .pipe(catchError(this.handleError<any>('getEvent')));
  }

  createEvent(eventDetail: IEventDetail): Observable<any> {
    const { name, location, date, expectedWeather } = eventDetail;

    return this.http
      .post<any>(
        this.eventsUrl,
        {
          name,
          location,
          date,
          expectedWeather,
        },
        this.httpOptions
      )
      .pipe(catchError(this.handleError<any>('createEvent')));
  }

  updateEvent(id: string, eventDetail: IEventDetail): Observable<any> {
    const { name, location, date, expectedWeather } = eventDetail;

    return this.http
      .put<any>(
        `${this.eventsUrl}/${id}`,
        {
          name,
          location,
          date,
          expectedWeather,
        },
        this.httpOptions
      )
      .pipe(catchError(this.handleError<any>('updateEvent')));
  }

  deleteEvent(id: string): Observable<any> {
    return this.http
      .delete<IEvent>(`${this.eventsUrl}/${id}`, this.httpOptions)
      .pipe(catchError(this.handleError<any>('deleteEvent')));
  }

  loadForecast(address: string): Observable<any> {
    return this.http.get<any>(`${this.geocodeUrl}&address=${address}`).pipe(
      concatMap((res) => {
        const { lat, lng } = res.results[0].geometry.location;
        return this.http
          .get<any>(`${this.weatherUrl}&lat=${lat}&lon=${lng}`)
          .pipe(catchError(this.handleError<any>('loadForecast', [])));
      }),
      catchError(this.handleError<any>('geocodeAddress', []))
    );
  }

  processError(message: string): void {
    this.alertService.create({
      title: 'Event Error',
      message,
      primaryActionLabel: 'Continue',
      onPrimaryActionClick: () => this.alertService.clear(),
    });
  }
}
