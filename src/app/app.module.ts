import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import Amplify from 'aws-amplify';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventsComponent } from './events/events.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { LoginComponent } from './login/login.component';
import {
  NgbActiveModal,
  NgbDatepickerModule,
  NgbModalModule,
  NgbAlertModule,
} from '@ng-bootstrap/ng-bootstrap';
import { AlertComponent } from './alert/alert.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { environment } from 'src/environments/environment';

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: environment.aws.region,
    userPoolId: environment.aws.userPoolId,
    userPoolWebClientId: environment.aws.userPoolWebClientId,
    authenticationFlowType: environment.aws.authenticationFlowType,
  },
});

@NgModule({
  declarations: [
    AppComponent,
    EventsComponent,
    EventDetailComponent,
    LoginComponent,
    AlertComponent,
    EditEventComponent,
    CreateEventComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbDatepickerModule,
    NgbModalModule,
    NgbAlertModule,
    HttpClientModule,
  ],
  providers: [NgbActiveModal],
  bootstrap: [AppComponent],
})
export class AppModule {}
