import { IEvent } from './event';

export const EVENTS: IEvent[] = [
  {
    id: 'evt-1',
    name: 'Intergalactic',
    date: new Date(2021, 9, 3),
    expectedWeather: 'Unknown',
    location: 'Moon',
  },
];
